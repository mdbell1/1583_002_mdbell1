public class CombatCalculator2
{
	public static void main(String [] args)
	{

	//moster data variables	
	String monster;
	monster = "goblin";

	int monsterHealth;
	monsterHealth = 100;

	int monsterPower;
	monsterPower = 15;

	//hero data variables
	int heroHealth;
	heroHealth = 100;

	int heroAttack;
	heroAttack = 12;

	int heroMagic;
	heroMagic = 0;

	//report combat stats
	System.out.println("You are fighting a " + monster);
	System.out.println("The monster HP: " + monsterHealth);
	System.out.println("Your HP: " + heroHealth);
	System.out.println("Your MP: " + heroMagic);

	}
}