import java.util.Scanner;
public class CombatCalculator3
{
	public static void main(String [] args)
	{

	//initialize scanner
	Scanner input = new Scanner( System.in );
	
	//moster data variables	
	String monster;
	monster = "goblin";

	int monsterHealth;
	monsterHealth = 100;

	int monsterPower;
	monsterPower = 15;

	//hero data variables
	int heroHealth;
	heroHealth = 100;

	int heroAttack;
	heroAttack = 12;

	int heroMagic;
	heroMagic = 0;

	//report combat stats
	System.out.println("You are fighting a " + monster);
	System.out.println("The monster HP: " + monsterHealth);
	System.out.println("Your HP: " + heroHealth);
	System.out.println("Your MP: " + heroMagic);

	//combat menu prompt
	System.out.println("Combat Options: ");
	System.out.println("1.) Sword Attack");
	System.out.println("2.) Cast Spell");
	System.out.println("3.) Charge Mana");
	System.out.println("4.) Run Away");

	//prompt play for action
	System.out.print("What action do you want to perform?");

	//declare variable for user input
	int choice;
	choice = input.nextInt();

	}
}