import java.util.Scanner;
public class CombatCalculator4
{
	public static void main(String [] args)
	{

	//initialize scanner
	Scanner input = new Scanner( System.in );
	
	//moster data variables	
	String monster;
	monster = "goblin";

	int monsterHealth;
	monsterHealth = 100;

	int monsterPower;
	monsterPower = 15;

	//hero data variables
	int heroHealth;
	heroHealth = 100;

	int heroAttack;
	heroAttack = 12;

	int heroMagic;
	heroMagic = 0;

	//report combat stats
	System.out.println("You are fighting a " + monster);
	System.out.println("The monster HP: " + monsterHealth);
	System.out.println("Your HP: " + heroHealth);
	System.out.println("Your MP: " + heroMagic);

	//combat menu prompt
	System.out.println("Combat Options: ");
	System.out.println("1.) Sword Attack");
	System.out.println("2.) Cast Spell");
	System.out.println("3.) Charge Mana");
	System.out.println("4.) Run Away");

	//prompt play for action
	System.out.print("What action do you want to perform? ");

	//declare variable for user input
	int choice;
	choice = input.nextInt();

	//if player chose option 1
	if (choice == 1)
		//print attack text
		System.out.println("You strike the " + monster + " with your sword for " + heroAttack + " damage");
	else if (choice == 2)
		//print spell message
		System.out.println("You cast the weaken spell on the monster.");
	else if (choice == 3)
		//print charging message
		System.out.println("You focus and charge your magic power.");
	else if (choice == 4)
		//print retreat message
		System.out.println("You run away!");
	else
		//print error message
		System.out.println("I don't understand that command.");

	}
}