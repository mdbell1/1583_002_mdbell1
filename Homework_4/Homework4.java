//Homework4
//Mia Bell
import java.util.Scanner;

class Homework4{
	public static void main(String[] args) {

		String[] roomDescription = new String[6]; //initialize statement

		roomDescription[0] = "You are in the front room, exit is north";
		roomDescription[1] = "You are in the Master Bedroom, exit is east";
		roomDescription[2] = "You are in the kitchen, exit is north, south,east and west";
		roomDescription[3] = "You are in the guest bedroom, exit is north and west";
		roomDescription[4] = "You are in the dining room, exit is south and east";
		roomDescription[5] = "You are in the Movie Room, exit is west and south";

		//create for loop
		/*
		*for (int i = 0; i < roomDescription.length; i++) {
		*	System.out.println(" " + roomDescription[i]);
		* }
		*/
		int[][] roomExits = {{2,-1,-1,-1}, {-1,2,-1,-1}, {4,3,1,0}, {5, -1, 2, -1}, {-1,5,-1,2}, {-1, -1,4,3}};

		int currentRoom = 0;

		final int NORTH = 0;
		final int EAST = 1;
		final int WEST = 2;
		final int SOUTH = 3;

		boolean toQuit = true;

		while (toQuit) {

			int directionToGo = 0;
			Scanner input = new Scanner(System.in);
			System.out.println(roomDescription[currentRoom]);
			System.out.print("Where do you want to go?  (Enter n,s,e,w):");
			String userResponse = input.next();
			

			if (userResponse.equals("n"))
				currentRoom = roomExits[currentRoom][NORTH];
			else if (userResponse.equals("e"))
				currentRoom = roomExits[currentRoom][EAST];
			else if (userResponse.equals("w"))
				currentRoom = roomExits[currentRoom][WEST];
			else if (userResponse.equals("s"))
				currentRoom = roomExits[currentRoom][SOUTH];	
			else if (userResponse.equals("q")){
				System.out.println("You are quitting!!!");
				toQuit = false; 
			}
			else 
				System.out.println("You enter inncorrect data.");

			if (currentRoom == -1)
				System.out.println("You ran into a wall!!!");

		} //end while loop

				
	} // end main method
} // end of ArrayRoom