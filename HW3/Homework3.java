import java.util.Scanner;
/*
* Mia Bell
* Spring 2017
*/

public class Homework3 {
	//hero stats
	private static int health = 0;
	private static int attackPower = 0;
	private static int magicPower = 0;

	//monster stats
	private static String monsterName = "";

	private static Scanner input = new Scanner(System.in);

public static void main(String [] args) {
		characterSelection();
	}

public static void characterSelection(){
		int points = 20;
		int options;
		//while loop
		while (points > 0) {

			//report user hero stat
			System.out.println("Health:" + health + "Attack:" + attackPower + "Magic:" + magicPower);

			//display user stats could purchase
			System.out.println("1) +10 Health");
			System.out.println("2)  +1 Attack");
			System.out.println("3)  +2 Magic");

			//report to user how many stat points
			System.out.print("You have " + points + " points to spend: ");

			//get user input
			options = input.nextInt();

			//update the hero's variable
			if (options == 1) {
				points--;
				health = health + 10;
			} else if (options == 2) {
				points--;
				attackPower++;
			} else if (options == 3) {
				points--;
				magicPower = magicPower + 3;
			} else {
				System.out.println("This is not a valid choice.");
			}

			System.out.println("Health:" + health + "Attack:" + attackPower + "Magic:" + magicPower);
		}
	}	
}