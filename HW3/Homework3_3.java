import java.util.Scanner;
import java.util.Random;
/*
* Mia Bell
* Spring 2017
*/

public class Homework3_3 {
	//hero stats
	private static int health = 0;
	private static int attackPower = 0;
	private static int magicPower = 0;

	private static boolean quit = false;

	//monster stats
	private static String monsterName = "";
	private static int monsterHealth;
	private static int monsterAttack;
	private static int xp;

	// import scanner and random
	private static Scanner input = new Scanner(System.in);
	private static Random rand = new Random ();

	public static void main(String [] args) {
		characterSelection();
		generateMonster();
		combatOptions();
		meleeOption();
		alive();
		magicOption();
		fleeOption();
	}

	public static void characterSelection(){
		int points = 20;
		int options;
		//while loop
		while (points > 0) {

			//report user hero stat
			System.out.println("Health:" + health + "Attack:" + attackPower + "Magic:" + magicPower);

			//display user stats could purchase
			System.out.println("1) +10 Health");
			System.out.println("2)  +1 Attack");
			System.out.println("3)  +2 Magic");

			//report to user how many stat points
			System.out.print("You have " + points + " points to spend: ");

			//get user input
			options = input.nextInt();

			//update the hero's variable
			if (options == 1) {
				points--;
				health = health + 10;
			} else if (options == 2) {
				points--;
				attackPower++;
			} else if (options == 3) {
				points--;
				magicPower = magicPower + 3;
			} else {
				System.out.println("This is not a valid choice.");
			}

			System.out.println("Health:" + health + "Attack:" + attackPower + "Magic:" + magicPower);
		}
	}	
	public static void generateMonster(){
		int monster = rand.nextInt(3);

		if (monster == 0) {
			monsterName = "goblin";
			monsterHealth = 75 + rand.nextInt(25);
			attackPower = 8 + rand.nextInt(5);
			xp = 1; 
		} else if (monster == 1){
			monsterName = "orc";
			monsterHealth = 100 + rand.nextInt(25);
			attackPower = 12+ rand.nextInt(5);
			xp = 3; 
		} else {
			monsterName = "troll";
			monsterHealth = 150 + rand.nextInt(25);
			attackPower = 15 + rand.nextInt(5);
			xp = 5;  
		} //end selection 

		
	// report monster
		System.out.println("You have encountered a " + monsterName);
		
		} 

	public static void combatOptions(){
		//report combat stats
		System.out.println("You are fighting a " + monster);
		System.out.println("The monster HP: " + monsterHealth);
		System.out.println("Your HP: " + heroHealth);
		System.out.println("Your MP: " + heroMagic);

		//combat menu prompt
		System.out.println("Combat Options: ");
		System.out.println("1.) Sword Attack");
		System.out.println("2.) Cast Spell");
		System.out.println("3.) Charge Mana");
		System.out.println("4.) Run Away");

		//prompt play for action
		System.out.print("What action do you want to perform? ");

		//declare variable for user input
		int choice;
		choice = input.nextInt();

		//if player chose option 1
		if (choice == 1) {
			//print attack text
			System.out.println("You strike the " + monster + " with your sword for " + heroAttack + " damage");
		}else if (choice == 2){
			//print spell message
			System.out.println("You cast the weaken spell on the monster.");
		}else if (choice == 3){
			//print charging message
			System.out.println("You focus and charge your magic power.");
		}else if (choice == 4) {
			//print retreat message
			System.out.println("You run away!");
		} else {
			//print error message
			System.out.println("I don't understand that command.");
		}
	}

	public static void meleeOption(){
		int damage = ran.nextInt(attackPower);
		monsterHealth = damage - monsterHealth;
		System.out.println("You attack the " + monsterName + "for " + damage + "damage");
		alive();
	}
	public static void alive(){
		if (monsterHealth < 0){
			int strikes = ran.nextInt(attackPower);
			System.out.println(monsterName + "strikes you for " + strikes + "damage");
		}
	}

	public static void magicOption(){
		if (monsterAttack > 0) {
		monsterHealth = monsterHealth/2;
		System.out.println("You cast a spell on the monster.");
		alive();
		} else{
			System.out.println("You can't do that because you don't have enough mana."); 
		}
	}

	public static void fleeOption(){
		// this is where I got stuck
	}
	
}//end class method