import java.util.Scanner;
import java.util.Random;
/*
* Mia Bell
* Spring 2017
*/

public class Homework3_2 {
	//hero stats
	private static int health = 0;
	private static int attackPower = 0;
	private static int magicPower = 0;

	private static boolean quit = false;

	//monster stats
	private static String monsterName = "";
	private static int monsterHealth;
	private static int monsterAttack;
	private static int xp;

	// import scanner and random
	private static Scanner input = new Scanner(System.in);
	private static Random rand = new Random ();

	public static void main(String [] args) {
		characterSelection();
		generateMonster();
	}

	public static void characterSelection(){
		int points = 20;
		int options;
		//while loop
		while (points > 0) {

			//report user hero stat
			System.out.println("Health:" + health + "Attack:" + attackPower + "Magic:" + magicPower);

			//display user stats could purchase
			System.out.println("1) +10 Health");
			System.out.println("2)  +1 Attack");
			System.out.println("3)  +2 Magic");

			//report to user how many stat points
			System.out.print("You have " + points + " points to spend: ");

			//get user input
			options = input.nextInt();

			//update the hero's variable
			if (options == 1) {
				points--;
				health = health + 10;
			} else if (options == 2) {
				points--;
				attackPower++;
			} else if (options == 3) {
				points--;
				magicPower = magicPower + 3;
			} else {
				System.out.println("This is not a valid choice.");
			}

			System.out.println("Health:" + health + "Attack:" + attackPower + "Magic:" + magicPower);
		}
	}	
	public static void generateMonster(){
		int monster = rand.nextInt(3);

		if (monster == 0) {
			monsterName = "goblin";
			monsterHealth = 75 + rand.nextInt(25);
			attackPower = 8 + rand.nextInt(5);
			xp = 1; 
		} else if (monster == 1){
			monsterName = "orc";
			monsterHealth = 100 + rand.nextInt(25);
			attackPower = 12+ rand.nextInt(5);
			xp = 3; 
		} else {
			monsterName = "troll";
			monsterHealth = 150 + rand.nextInt(25);
			attackPower = 15 + rand.nextInt(5);
			xp = 5;  
		} //end selection 

		
	// report monster
		System.out.println("You have encountered a " + monsterName);
		
	} 
}//end class method