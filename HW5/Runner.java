import java.util.Scanner;
public class Runner {
	public static int direction;
	private static int roomNumber;
	private static int currentRoom;
	private static boolean toQuit = true;
	private static RoomDescription description = new RoomDescription(currentRoom);
	private static roomExits exits = new roomExits(currentRoom, direction);

	public static void main (String[] args) {
		while (toQuit){
			userResponse();
			roomNumber = exits.nextRoom(currentRoom, direction);
			if (roomNumber == -1)
				System.out.println("You hit a damn wall guy!!!");
			else if(roomNumber >= 0 && roomNumber < 99)
				currentRoom = roomNumber; 

		}// end while loop

	} //end main method

	public static void userResponse() {
			Scanner input = new Scanner(System.in);
			System.out.println(description.location(currentRoom));
			System.out.print("Where do you want to go?  (Enter n,s,e,w):");
			String userResponse = input.next();
			

			if (userResponse.equals("n"))
				direction = 0;
			else if (userResponse.equals("e"))
				direction = 1;
			else if (userResponse.equals("w"))
				direction = 2;
			else if (userResponse.equals("s"))
				direction = 3;	
			else if (userResponse.equals("q"))
				direction = 4;
	} // end userResponse
} // end class Runner