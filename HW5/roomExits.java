public class roomExits {
	protected int currentRoom;
	protected int newRoom;
	protected int direction;

	public roomExits(int currentRoom, int direction) {
		this.currentRoom = currentRoom;
		this.direction = direction; 
	} // end constructor

	public int nextRoom(int currentRoom, int direction) {
		int[][] roomExits = {{2,-1,-1,-1}, {-1,2,-1,-1}, 
		{4,3,1,0}, {5, -1, 2, -1}, {-1,5,-1,2}, {-1, -1,4,3}};

		if (direction < 3) {
		newRoom = roomExits[currentRoom][direction];
		return newRoom; 
		}
		else
			return 99; 
	}
} //end class roomExits