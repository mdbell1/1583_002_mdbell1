public class RoomDescription{
	protected int currentRoom;

	public RoomDescription(int currentRoom){
		this.currentRoom = currentRoom;
	} //end of constructor

	public String location (int currentRoom){
	String[] roomDescription = new String[6]; //initialize statement

		roomDescription[0] = "You are in the front room, exit is north";
		roomDescription[1] = "You are in the Master Bedroom, exit is east";
		roomDescription[2] = "You are in the kitchen, exit is north, south,east and west";
		roomDescription[3] = "You are in the guest bedroom, exit is north and west";
		roomDescription[4] = "You are in the dining room, exit is south and east";
		roomDescription[5] = "You are in the Movie Room, exit is west and south";

		return roomDescription[currentRoom];
	}


} //end class RoomDescription